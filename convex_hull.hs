import Data.List (sortBy, elemIndex)

data Direction = LeftTurn | RightTurn | NoTurn deriving (Eq, Show)
data Point = Point {x::Int, y::Int} deriving (Eq, Show)

ccw::Point->Point->Point->Direction
ccw p q r  | res == 0 = NoTurn 
           | res < 0  = LeftTurn
           | res > 0  = RightTurn
            where res = ((y q) - (y p)) * ((x r) - (x q)) - ((x q) - (x p)) * ((y r) - (y q))

calculateSuccessiveTurns::[Point]->[Direction]
calculateSuccessiveTurns (a:b:c:[]) = (ccw a b c) : []
calculateSuccessiveTurns (a:b:c:xs) = (ccw a b c) : (calculateSuccessiveTurns (b:c:xs))
calculateSuccessiveTurns (a:_) = []

type PointAndLocation = (Point, Int)
--finds the lowest point
findLowestPoint::[Point]->PointAndLocation
findLowestPoint (e:[]) = (e, 0)
findLowestPoint (e:es) = let v = min' e (fst (findLowestPoint es)) 
                             min' a b | ((y a) < (y b)) = a
                                      | ((y a) > (y b)) = b
                                      | ((x a) <= (x b)) = a
                         in (v, case (elemIndex v (e:es)) of 
                                     Just i -> i 
                                     Nothing -> (-1))

--swap the first point with the lowest point so that the lowest point becomes the first element in the list
swapPoints::[Point]->[Point]
swapPoints pl = let (p, pos) = findLowestPoint pl
                    in p:((take pos pl) ++ (drop (pos+1) pl))

sort'::Point->[Point]->[Point]
sort' lowestPoint l = sortBy f l
                      where f = \p1 p2->
                                 let t = ccw lowestPoint p1 p2
                                     --calculates distance between two points
                                     dist::Point->Point->Int
                                     dist a b = ((x a) - (x b)) *  ((x a) - (x b)) + ((y a) - (y b)) *  ((y a) - (y b))
                                 in if t == NoTurn then
                                     if (dist lowestPoint p2) >= (dist lowestPoint p1) then LT else GT
                                    else if t == LeftTurn then LT else GT

--sorts the points (uses sort' internally)
sort''::[Point]->[Point]
sort'' (x:xs) = x:(sort' x xs)

setup::[Point]->[Point]
setup lps =  let (stack, remainingList) =  setupStack (sort'' (swapPoints lps))
             in loop stack remainingList
                where loop s (l:[]) = push l (isTurn (ccw (nextToTop s) (top s) l) s l)
                      loop s (l:ls) = loop (push l (isTurn (ccw (nextToTop s) (top s) l) s l)) ls

isTurn LeftTurn s p = s
isTurn _        s p = let ps = pop s
                       in isTurn (ccw (nextToTop ps) (top ps) p) ps p

setupStack (a:b:c:xs) = ([a,b,c], xs)
push e l = l ++ [e]
pop = init 
top = last
nextToTop s = (take 2 (reverse s)) !! 1  
